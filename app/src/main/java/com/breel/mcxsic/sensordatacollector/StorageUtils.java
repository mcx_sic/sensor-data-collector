package com.breel.mcxsic.sensordatacollector;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static android.R.attr.data;

/**
 * Util to save info into storage
 * Created by michelcomin on 1/30/17.
 */

class StorageUtils {


    private static final String TAG = "StorageUtils";

    /**
     * Saves into corresponding file the new data for each sensor.
     */
    static boolean saveBundleData(Context context, Map<String, Map<String, Pair<Float, Long>>> dataBundle) {
        String fileName;
        File filePath;
        File f;
        FileWriter writer;
        String[] data;
        String dataArray;
        Pair<Float, Long> pairVal;

        try {
            for (Map.Entry<String, Map<String, Pair<Float, Long>>> sensorGroup : dataBundle.entrySet()) {
                filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                fileName = sensorGroup.getKey() + ".csv"; // Name of the sensor group.

                Log.d(TAG, "File Path: " + filePath);
                Log.d(TAG, "File Name: " + fileName);

                f = new File(filePath, fileName);

                /* Check if file exists. */
                if (f.exists() && !f.isDirectory()) {
                    Log.d(TAG, "File Exists");
                    f.delete();
                    writer = new FileWriter(f, true);
                } else {
                    f.mkdirs();
                    Log.d(TAG, "File doesn't exist; Creating a new one");
                    writer = new FileWriter(f);
                }

                /* Write values. */
                for (Map.Entry<String, Pair<Float, Long>> sensorVal : sensorGroup.getValue().entrySet()) {
                    pairVal = sensorVal.getValue();

                    data = new String[3];
                    data[0] = sensorVal.getKey(); // name/ID of the sensor.
                    data[1] = pairVal.first.toString(); // The actual value.
                    data[2] = pairVal.second.toString(); // The Timestamp (in millis).

                    dataArray = Arrays.toString(data);
                    dataArray = dataArray.substring(1, dataArray.length() - 1) + "\n";
                    Log.d(TAG, "Saving sensor values into " + fileName + ": " + dataArray + " /// " + Arrays.toString(data));

                    writer.write(dataArray);
                }

                writer.close();

                StorageUtils.callFilesBroadcast(context, f);

            }
        } catch (Exception e) {
            Log.e(TAG, "Sensors data haven't been saved. " + e);

            return false;
        }

        return true;
    }


    /**
     * Retrieves into corresponding file the new data for each sensor.
     */
    static boolean retrieveData(String fileId, Map<String, List<Pair<Float, Long>>> data) {
        String fileName;
        File filePath;
        File f;

        try {
            filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            fileName = fileId + ".csv"; // Name of the sensor group.

            Log.d(TAG, "File Path: " + filePath);
            Log.d(TAG, "File Name: " + fileName);

            f = new File(filePath, fileName);

                /* Check if file exists. */
            if (f.exists() && !f.isDirectory()) {
                Log.d(TAG, "File Exists");
                FileInputStream fis;
                fis = new FileInputStream(f);
                StringBuilder fileContent = new StringBuilder("");
                byte[] buffer = new byte[1024];
                int n;

                while ((n = fis.read(buffer)) != -1) {
                    String dataLine = new String(buffer, 0, n);
                    fileContent.append(dataLine);
                    String[] lines = dataLine.split("\n");
                    for (String line : lines) {
                        putDataLine(line, data);
                    }

                }

//                Log.d(TAG, fileContent.toString());

            } else {
                Log.d(TAG, "File doesn't exist; Creating a new one");
                return false;
            }

        } catch (Exception e) {
            Log.e(TAG, "Sensors data haven't been saved. " + e);
            return false;
        }

        return true;
    }

    private static void putDataLine(String dataLine, Map<String, List<Pair<Float, Long>>> data) {
        String[] values = dataLine.replaceAll(" ", "").split(",");
//        Log.d(TAG, "Values Length: " + Arrays.toString(values));
        if (values.length == 3) {
            String id = values[0];
            float value = Float.valueOf(values[1]);
            long timestamp = Long.valueOf(values[2]);
            List<Pair<Float, Long>> valuesList;
            if (data.containsKey(id)) {
                valuesList = data.get(id);
            } else {
                valuesList = new ArrayList<>();
            }
            valuesList.add(new Pair<>(value, timestamp));
            data.put(id, valuesList);

            Log.d(TAG, "Line read: " + id + " - Value: " + value + " - timestamp: " + timestamp);
        }
    }

    private static void callFilesBroadcast(Context context, File file) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));
        context.sendBroadcast(intent);
    }
}
