package com.breel.mcxsic.sensordatacollector;

import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Class with different Utils.
 * Created by michelcomin on 1/30/17.
 */
class Utils {
    static String getDate(long timeInMillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        return DateFormat.format("dd/MM/yyyy hh:mm a", cal).toString();
    }
}
