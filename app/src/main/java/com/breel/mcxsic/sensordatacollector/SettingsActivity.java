package com.breel.mcxsic.sensordatacollector;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String SENSORS_FREQUENCY = "sensors_frequency";
    public static final String SENSORS_ACTIVATED = "sensors_activated";
    public static final String SELECTED_SENSORS = "selected_sensors";
    public static final String AMBIENT_ENABLED = "ambient_enabled";
    public static final String MOTION_ENABLED = "motion_enabled";
    public static final String POSITION_ENABLED = "position_enabled";
    private static final int PERMISSIONS_REQUEST = 0;
    public static final String PERMISSIONS_GRANTED = "permissions_granted";

    private boolean arePermissionsGranted = false;

    public Set<String> selectedSensors;

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();
            Log.d("Value selected", stringValue);
            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };


    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(preference.getContext());
        sBindPreferenceSummaryToValueListener.onPreferenceChange(
                preference,
                sp.getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

        /* Set Shared Preferences Listener */
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        //Retrieve selected Sensors
        selectedSensors = sharedPref.getStringSet(SELECTED_SENSORS, new HashSet<String>());
        boolean sensorActive = sharedPref.getBoolean(SENSORS_ACTIVATED, false);
        arePermissionsGranted = sharedPref.getBoolean(PERMISSIONS_GRANTED, false);

        /* Set Alarm Scheduler */
        SensorReceiverService.setAlarmScheduler((AlarmManager) getSystemService(Context.ALARM_SERVICE));
        /* Set Sensor Manager */
        SensorReceiverService.setSensorManager((SensorManager) getSystemService(SENSOR_SERVICE));
        /* Set Sensor Manager */
        SensorReceiverService.setEnabledSensors(selectedSensors);
        /* Set permissions granted. */
        SensorReceiverService.setPermissionsGranted(arePermissionsGranted);

        sharedPref.registerOnSharedPreferenceChangeListener(this);

        if (sensorActive)
            SensorReceiverService.scheduleDataCollector(getApplicationContext(), 10 * 1000);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new DataSyncPreferenceFragment())
                .commit();
    }

    @Override
    protected void onDestroy() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        sharedPref.unregisterOnSharedPreferenceChangeListener(this);
        Log.d("SettingsActivity", "=== destroyed ===");
        super.onDestroy();
    }


    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle(R.string.app_name);
            actionBar.setSubtitle(R.string.subtitle);
        }
    }


    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        boolean sensorActive = false;
        if (SettingsActivity.SENSORS_ACTIVATED.equals(key) ||
                SettingsActivity.AMBIENT_ENABLED.equals(key) ||
                SettingsActivity.POSITION_ENABLED.equals(key) ||
                SettingsActivity.MOTION_ENABLED.equals(key)) {
            sensorActive = sharedPreferences.getBoolean(key, false);
            Log.d("SettingsActivity", "Sensors " + key + " Active: " + sensorActive);
        }
        switch (key) {
            case SettingsActivity.SENSORS_ACTIVATED:
                if (sensorActive) {
                    checkPermissions();
                    SensorReceiverService.scheduleDataCollector(getApplicationContext(), 10 * 1000);
                } else
                    SensorReceiverService.stopDataCollector(getApplicationContext());
                break;

            case SettingsActivity.AMBIENT_ENABLED:
                if (sensorActive)
                    selectedSensors.add(SensorsGroups.AMBIENT_SENSORS_ID);
                else
                    selectedSensors.remove(SensorsGroups.AMBIENT_SENSORS_ID);

                updateSensorsActive(sharedPreferences);
                break;

            case SettingsActivity.POSITION_ENABLED:
                if (sensorActive)
                    selectedSensors.add(SensorsGroups.POSITION_SENSORS_ID);
                else
                    selectedSensors.remove(SensorsGroups.POSITION_SENSORS_ID);

                updateSensorsActive(sharedPreferences);
                break;

            case SettingsActivity.MOTION_ENABLED:
                if (sensorActive)
                    selectedSensors.add(SensorsGroups.MOTION_SENSORS_ID);
                else
                    selectedSensors.remove(SensorsGroups.MOTION_SENSORS_ID);

                updateSensorsActive(sharedPreferences);
                break;
        }
    }


    /**
     * Grant permissions to write on External Storage
     */
    private void checkPermissions() {
        if (arePermissionsGranted)
            return;

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST);
        }
    }


    /**
     * Updates the selected sensors
     *
     * @param sharedPref
     */
    private void updateSensorsActive(SharedPreferences sharedPref) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(SELECTED_SENSORS, selectedSensors);
        editor.apply();
        SensorReceiverService.setEnabledSensors(selectedSensors);
    }


    /**
     * This fragment shows data and sync preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DataSyncPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(SENSORS_FREQUENCY));
        }
    }


    /**
     * Callback for when permissions dialog has finished
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST:
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = sharedPref.edit();
                // If request is cancelled, the result arrays are empty.
                boolean granted = grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED;

                editor.putBoolean(PERMISSIONS_GRANTED, granted);
                editor.apply();

                arePermissionsGranted = granted;
                SensorReceiverService.setPermissionsGranted(arePermissionsGranted);

                break;
        }
    }
}
