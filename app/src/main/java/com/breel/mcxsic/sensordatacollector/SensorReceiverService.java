package com.breel.mcxsic.sensordatacollector;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Environment;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class SensorReceiverService extends IntentService implements SensorEventListener {
    private static final String TAG = "SensorReceiverService";
    private static final String ACTION_START_SENSOR = "com.breel.mcxsic.sensordatacollector.action.ACTION_START_SENSOR";


    private static SensorManager sensorManager = null;
    private static AlarmManager scheduler = null;
    private static Set<String> enabledSensors = null;
    private Map<String, Sensor> currentSensorsListening = null;
    private Map<String, Map<String, Pair<Float, Long>>> dataBundle;
    private static boolean permissionsGranted;


    /**
     * Static method that sets the alarm scheduler.
     * @param scheduler the alarm scheduler.
     */
    public static void setAlarmScheduler(AlarmManager scheduler) {
        if (SensorReceiverService.scheduler == null)
            SensorReceiverService.scheduler = scheduler;
    }


    /**
     * Static method that sets the sensor manager
     * @param sensorManager the current sensor manager
     */
    public static void setSensorManager(SensorManager sensorManager) {
        if (SensorReceiverService.sensorManager == null)
            SensorReceiverService.sensorManager = sensorManager;
    }


    /**
     * Static method that sets the enabled Sensors that they need to get the info.
     * @param enabledSensors an array with the different types of sensors (see @SettingsActivity.java)
     */
    public static void setEnabledSensors(Set<String> enabledSensors) {
        SensorReceiverService.enabledSensors = enabledSensors;
    }


    /**
     * Indicates if permissions has been granted for writing in external storage.
     * @param permissionsGranted
     */
    public static void setPermissionsGranted(boolean permissionsGranted) {
        SensorReceiverService.permissionsGranted = permissionsGranted;
    }


    /**
     * Static method that stops the data collector scheduler.
     * @param context the App context.
     */
    public static void stopDataCollector(Context context) {
        PendingIntent scheduledIntent = PendingIntent.getService(
                context,
                0,
                generateSensorIntent(context),
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (scheduler != null) {
            scheduler.cancel(scheduledIntent);
        }
    }


    /**
     * Static method to schedule the DataCollectorService
     * @param context the App context
     * @param interval the interval when we want to wake up the service to get the Sensors Data.
     */
    public static void scheduleDataCollector(Context context, long interval) {
        Log.d(TAG, "Set Scheduler");
        PendingIntent scheduledIntent = PendingIntent.getService(
                context,
                0,
                generateSensorIntent(context),
                PendingIntent.FLAG_UPDATE_CURRENT); // FLAG_UPDATE_CURRENT will update the current service if any

        scheduler.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(),
                interval,
                scheduledIntent);
    }


    /**
     * Creates an IntentService. Invoked by your subclass's constructor.
     */
    public SensorReceiverService() {
        super("SensorReceiverService");
    }


    /**
     * onCreate
     */
    @Override
    public void onCreate() {
        super.onCreate();
        dataBundle = new HashMap<>();
        Log.d(TAG, "Service created");
    }


    /**
     * Called when a new command starts the service
     * @param intent the new intent
     * @param flags the current flags
     * @param startId the start ID
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        /* Handle the new intent. */
        onHandleIntent(intent);

        /* Return START_STICKY to keep service on until we don't stop it. */
        return START_STICKY;
    }


    /**
     * onDestroy
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        /* Stop doing. */
        if (sensorManager != null) {
            sensorManager.unregisterListener(this);
        }

        if (SensorReceiverService.permissionsGranted &&
                currentSensorsListening.size() < 1) {
            StorageUtils.saveBundleData(getApplicationContext(), dataBundle);
            Log.d(TAG, "Data Saved");
            // FIXME: to remove
//            StorageUtils.retrieveData(SensorsGroups.AMBIENT_SENSORS_ID, new HashMap<String, List<Pair<Float, Long>>>());
        }

        Log.d(TAG, "Service destroyed");
    }


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static Intent generateSensorIntent(Context context) {
        Intent intent = new Intent(context, SensorReceiverService.class);
        intent.setAction(ACTION_START_SENSOR);
        return intent;
    }


    /**
     * Called when service is called using a new intent.
     * @param intent the new intent received
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "intent Called");
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START_SENSOR.equals(action)) {
                handleSensorInfo();
            }
        }
    }


    /**
     * Sensor method for when the sensor has changed
     * @param event the sensor event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        long timeInMillis = System.currentTimeMillis();
        String date = Utils.getDate(timeInMillis);

        if (currentSensorsListening.containsKey(event.sensor.getStringType())) {
            String dataArray = Arrays.toString(event.values);
            dataArray = dataArray.substring(1, dataArray.length() - 1).replace(" ", "");
            Log.d(TAG, "Sensor: " + event.sensor.getStringType() + " - Value: " + dataArray + " - timestamp: " + date);

            /* Save the value into cache. */
            saveValue(event, timeInMillis);

            /* Remove current sensor from list of sensors to get data */
            currentSensorsListening.remove(event.sensor.getStringType());
            if (currentSensorsListening.size() < 1)
                /* If all the sensors have new data, stop the service. */
                stopSelf();
        }
    }


    /**
     * Sensor method for when the accuracy has changed
     * @param sensor   the current sensor
     * @param accuracy the new accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }


    /**
     * Prepare the sensors array that we want to get info.
     * parameters.
     */
    private void handleSensorInfo() {
        if (sensorManager == null)
            return;

        if (currentSensorsListening != null &&
                currentSensorsListening.size() > 0 &&
                sensorManager != null)
            sensorManager.unregisterListener(this);

        currentSensorsListening = new ArrayMap<>();

        /* Get the sensors list according to the list of Sensors types that we want info. */
        for (String enabledSensorGroup : enabledSensors) {
            setSensorsList(enabledSensorGroup, currentSensorsListening);
        }

        /* Register all the sensors that we want info. */
        registerSensorsListeners();
    }


    /**
     * Set the sensors for each sensor type that we are registering.
     * i.e: AMBIENT = light, pressure, humidity and temperature sensors.
     * @param enabledSensorGroup the group of sensors to activate.
     * @param currentSensorsListening Map that contains all the active sensors.
     */
    private void setSensorsList(String enabledSensorGroup, Map<String, Sensor> currentSensorsListening) {
        Log.d(TAG, "Setting " + enabledSensorGroup);
        switch (enabledSensorGroup) {
            case SensorsGroups.AMBIENT_SENSORS_ID:
                setSensorsForSensorGroup(
                        SensorsGroups.AMBIENT_SENSORS_ID,
                        SensorsGroups.AMBIENT_SENSORS_LIST,
                        currentSensorsListening);
                break;

            case SensorsGroups.POSITION_SENSORS_ID:
                setSensorsForSensorGroup(
                        SensorsGroups.POSITION_SENSORS_ID,
                        SensorsGroups.POSITION_SENSORS_LIST,
                        currentSensorsListening);
                break;

            case SensorsGroups.MOTION_SENSORS_ID:
                setSensorsForSensorGroup(
                        SensorsGroups.MOTION_SENSORS_ID,
                        SensorsGroups.MOTION_SENSORS_LIST,
                        currentSensorsListening);
                break;
        }
    }


    /**
     * Set the related sensors to a sensor group the the list of sensors to get information.
     * @param id The ID of the sensor group.
     * @param sensors The sensors related to the sensor group.
     * @param currentSensorsListening The current list of senors that we are listening.
     */
    private void setSensorsForSensorGroup(String id, int[] sensors, Map<String, Sensor> currentSensorsListening) {
        for (int sensorType : sensors) {
            Sensor sensor = sensorManager.getDefaultSensor(sensorType);

            if (sensor != null) {
                Log.d(TAG, "Sensor: " + sensor.getStringType());
                currentSensorsListening.put(sensor.getStringType(), sensor);
            } else {
                Log.e(TAG, "The sensor with ID=" + sensorType + " is not available on the current device.");
            }
        }
        dataBundle.put(id, new HashMap<String, Pair<Float, Long>>());
    }


    /**
     * Register the sensor listeners.
     */
    private void registerSensorsListeners() {
        for (Map.Entry<String, Sensor> sensor : currentSensorsListening.entrySet())
            sensorManager.registerListener(this, sensor.getValue(), SensorManager.SENSOR_DELAY_NORMAL);
    }


    /**
     * Saves the sensor value into a cache objects.
     * @param event The sensor event.
     * @param timeInMillis The timestamp in milliseconds.
     */
    private void saveValue(SensorEvent event, long timeInMillis) {
        String idBundle = SensorsGroups.getRelatedBundleId(event.sensor.getType());
        Log.d(TAG, "Bundle ID: " + idBundle + " for sensor " + event.sensor.getStringType());
        if (idBundle != null) {
            Map<String, Pair<Float, Long>> sensorData = dataBundle.get(idBundle);
            sensorData.put(event.sensor.getStringType(), new Pair<>(event.values[0], timeInMillis));
            dataBundle.put(idBundle, sensorData);
        }
    }
}
