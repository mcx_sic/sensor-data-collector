package com.breel.mcxsic.sensordatacollector;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;

import static com.breel.mcxsic.sensordatacollector.SettingsActivity.SELECTED_SENSORS;

/**
 * Created by michelcomin on 1/25/17.
 */
public class ActionsReceiver extends BroadcastReceiver{
    private static final String TAG = ActionsReceiver.class.toString();

    public ActionsReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case "android.intent.action.BOOT_COMPLETED":
                onBootCompleted(context, intent);
                break;
            case "android.intent.action.CONFIGURATION_CHANGED":
                onConfigChanged(context, intent);
                break;
            case "android.intent.action.PACKAGE_ADDED":
                onPackageInstalled(context, intent);
                break;
            case "android.intent.action.USER_PRESENT":
                onUserPresent(context, intent);
                break;
        }
    }


    /**
     * Called when the user is present
     * @param context the application context.
     * @param intent the intent who called the action.
     */
    private void onUserPresent(Context context, Intent intent) {
        //
    }


    /**
     * Called when package is installed.
     * @param context the application context.
     * @param intent the intent who called the action.
     */
    private void onPackageInstalled(Context context, Intent intent) {
        Log.d(TAG, "Package installed");
    }

    /**
     * Called when boot has finished.
     * @param context the application context.
     * @param intent the intent who called the action.
     */
    private void onBootCompleted(Context context, Intent intent) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        // Retrieve saved info
        boolean sensorActive = sharedPref.getBoolean(SettingsActivity.SENSORS_ACTIVATED, false);
        Set<String> selectedSensors = sharedPref.getStringSet(SELECTED_SENSORS, new HashSet<String>());
        boolean arePermissionsGranted = sharedPref.getBoolean(SettingsActivity.PERMISSIONS_GRANTED, false);

        /* Set Alarm Scheduler */
        SensorReceiverService.setAlarmScheduler((AlarmManager) context.getSystemService(Context.ALARM_SERVICE));
        /* Set Sensor Manager */
        SensorReceiverService.setSensorManager((SensorManager) context.getSystemService(SettingsActivity.SENSOR_SERVICE));
        /* Set Sensor Manager */
        SensorReceiverService.setEnabledSensors(selectedSensors);
        /* Set permissions granted. */
        SensorReceiverService.setPermissionsGranted(arePermissionsGranted);


        if (sensorActive) {
            SensorReceiverService.scheduleDataCollector(context, 10 * 1000);
        }
    }

    /**
     * Check what happens when config has changed
     * @param context the application context.
     * @param intent the intent who called the action.
     */
    private void onConfigChanged(Context context, Intent intent) {
        Log.d(TAG, "Config changed");
    }
}
