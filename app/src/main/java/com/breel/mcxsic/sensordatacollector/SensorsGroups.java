package com.breel.mcxsic.sensordatacollector;

import android.hardware.Sensor;

import java.util.Arrays;

/**
 * Contains info of all the sensors.
 * Created by michelcomin on 1/30/17.
 */

class SensorsGroups {
    static final String AMBIENT_SENSORS_ID = "ambient_sensors";
    static final int[] AMBIENT_SENSORS_LIST = {Sensor.TYPE_AMBIENT_TEMPERATURE, Sensor.TYPE_LIGHT, Sensor.TYPE_PRESSURE, Sensor.TYPE_RELATIVE_HUMIDITY};

    static final String MOTION_SENSORS_ID = "motion_sensors";
    static final int[] MOTION_SENSORS_LIST = {Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_GRAVITY, Sensor.TYPE_GYROSCOPE, Sensor.TYPE_SIGNIFICANT_MOTION, Sensor.TYPE_STEP_COUNTER, Sensor.TYPE_STEP_DETECTOR};

    static final String POSITION_SENSORS_ID = "position_sensors";
    static final int[] POSITION_SENSORS_LIST = {Sensor.TYPE_MAGNETIC_FIELD, Sensor.TYPE_GAME_ROTATION_VECTOR, Sensor.TYPE_PROXIMITY};

    static String getRelatedBundleId(int sensorType) {
        if (!Arrays.asList(AMBIENT_SENSORS_LIST).contains(sensorType)) {
            return AMBIENT_SENSORS_ID;
        }

        return null;
    }
}
